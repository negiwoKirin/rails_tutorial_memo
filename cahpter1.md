# ゼロからデプロイまで
## サーバー立ち上げ
### 詰まった点
rails serverコマンド
 - rails server 
 - rails server -b 0.0.0.0
 - rails s -b $IP -p $PORT

上記３点試したがダメ

クッキーの問題
>Enable third-party cookies in your web browser, and then try opening the environment again. For more information, see your web browser’s documentation.

chromeにブラウザを変えて解消した
## MVCモデル
railsではMVCモデルが基本。
サンプルファイルではapp/の中に「models」「views」「controllers」が存在している。

## Hello,World
デフォルトのページから差し替えたいのでrouterを編集する。
>Rails.application.routes.draw do
>
>root 'application#hello' 
>
>end
## git
Gitを初めて使うときコンピューター1台につき１度だけ設定しなければいけない

    $ git config --global user.name "Your Name"
    $ git config --global user.email your.email@example.com

### git iniでセットアップ
リポジトリを作成する。
アプリケーションのルートディレクトリ（今回はhello_app）に移動し実行。

    $ git init
    Initialized empty Git repository in
    /home/ec2-user/environment/environment/hello_app/.git/

次にgit add -Aでリポジトリにファイルを追加する。

    $ git add -A


Gitにプロジェクトのファイルを追加するとまずはステージングという状態になる。
git statusで状態を知る。

    $ gir status

リポジトリに反映（コミット）するにはcommitを使う。
-mフラグを使うとコミットメッセージをコマンドライン上で指定できる。

    $ git commit -m "initialize repository"

git logコマンドでメッセージ履歴を参照できる。

変更前の状態に強制上書き

    $ git checkout -f

### Branch(ブランチ)

トピックブランチ（一時的に使う短期的なブランチ）
checkout と-bフラグを使う。
-bの後でトピックブランチ名を指定。

    $ git checkout -b modify-README

git branchで全てのローカルブランチを表示する。
＊が付いているところが現在地。

    $ git branch
    master
    * modify-README


### Merge

masterBranchに変更をmergeする。

まずmasterBranchをcheckoutし
git merge [ブランチ名] でマージしたいブランチを指定する。

    $ git checkout master
    Switched to branch 'master'
    $ git merge modify-README
    Updating af72946..9dc4f64
    Fast-forward
     README.md | 27 +++++----------------------
    1 file changed, 5 insertions(+), 22 deletions(-)

変更をマージした後はgit branch -dでトピックブランチを削除する

    $ git branch -d modify-README

### Push（プッシュ）

さあプッシュをするのじゃ！！

    $ git push

## デプロイ
本番デプロイの練習をしましょう。

### herokuを使う

はまったエラー
heroku loginで無限ローディング

    $ heroku login --interactive

これで回避。

    $ heroku create

で出たアドレスにアクセスしてもうまくいかない場合は

    $ heroku open


◉macで「¡」（逆！）はoption + 1
