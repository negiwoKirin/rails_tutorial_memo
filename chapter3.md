# 第３章 ほぼ静的なページの作成

## 3.2 静的ページ
> 一言でまとめると、コントローラとは (基本的に動的な) Webページの集合を束ねるコンテナのことです。

### 3.2.1 静的なページの生成
generateスクリプトでコントローラーを生成する。
generateスクリプトではまとめて指定できるので今回はStaticPagesにhome,helpアクションを生成。

    $  rails generate controller StaticPages home help
.

    static_pages_controller.rb

        class StaticPagesController < ApplicationController
          def home
        end

        def help
        end
    end


◉`rails g`で`rails generate`との短縮系。
Railsではファイル名はスネーク記法が主。
`こまめにpush！！`

## コラム3.1 元に戻す方法
Railsには開発上の失敗をカバーする機能がいくつもある。
### 生成したコードを元に戻したい場合
例えばコントローラー名の変更。
この場合には`rails destroy`コマンドで取り消し処理。
以下の2つは自動生成とそれに対応する取り消しコマンド。

     $ rails generate controller StaticPages home help
      $ rails destroy  controller StaticPages home help

### マイグレーションを戻す
    $ rails db :migrate
に対し

    $ rails db :rollback
これで1つ目の状態に戻す。

    $ rails db :migrate VERSION=0
これで最初の状態に戻す。
みたまま、バージョンの数字は毎回振られるので、指定可能。


### 閑話休題
    Rails.application.routes.draw do
      get  'static_pages/home'
      get  'static_pages/help'
      root 'application#hello'
    end

get リクエストでアクセスしている。

### コラム3.2
GETは主にページを取得する際に用いられる。
POSTはフォームで入力した情報などに使う。

### 閑話休題

    static_pages_controller.rb

        class StaticPagesController < ApplicationController
          def home
        end

        def help
        end
    end

ApplicationControllerを継承しているのでhome,helpアクションのビューの呼び出しに対応。

## テスト
Railsのテストは簡単なコマンドで実行できる。

    $ rails test
### 3.3.2 Red

テスト駆動開発サイクルは「失敗するテストを最初に書く」「次にアプリケーションのコードを書いて成功させる」「必要ならリファクタリングする」
失敗をRed
成功をGreenで表す。
Red,Green,PERFACTORと呼ぶこともある。

◉`エラーメッセージをしっかり見よう！`

### 3.3.4 Refactor
システムが大きくなったり古くなるとどこからともなく腐敗臭が漂ってくるから瑞々しく新鮮に保とう。

## 3.4 少しだけ動的なページ
ページ内容に応じて、ページのタイトルを自ら書き換えるようにする。

ここでの目標はtitleタグの内容変更

### Rubyメソッドの埋め込み
    <%provide(:title,"hoge")%>

### guardfile
変更した点
.gitignoreに以下を追加。
    # Ignore Spring files.
    /spring/*.pid

注意点
springは不安定なので必要に応じてタスクキル。

    $ ps aux | grep spring

    $ kill -15 12241

一括キルを行うためにプロセス停止

    $ spring stop

これ効かない時

    $ pkill -15 -f spring


> 開発中に動作がおかしくなったりプロセスがフリーズしていると思えたら、すぐにps auxで状態を確認し、kill -15 < pid >やpkill -15 -f <プロセス名>で整理してみましょう。