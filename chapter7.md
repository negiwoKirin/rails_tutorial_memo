# 第７章　ユーザー登録
前章でUserモデルを作成したので、ユーザー登録機能を追加する。
-  HTMLフォームを使ってwebアプリケーションに登録情報を送信する。
- 情報をDBに保存する。
- ユーザーを表示するためのページ作成。RESTアーキテクチャの実装に入る。

## 7.1 ユーザーを表示する
[ユーザー表示のモックアップ](https://railstutorial.jp/chapters/images/figures/profile_mockup_profile_name_bootstrap.png "モックアップ ")

## 7.1.1 デバッグとRails環境
### デバッグ情報の追加
views/layout/application.html.erbにデバッグ情報を追加する。

         <%= debug(params) if Rails.env.development? %>

このifでは開発環境でのみ表示させるようにするためのもの。
本番アプリケーションやテストで挿入されることはない。

> ## コラム7.1 Railsの3つの環境
>Railsにはテスト環境（test）、開発環境（development）、本番環境（production）。Railsコンソールではdevelopment。

### Saasのミックスイン機能
`@mixin`で変数を指定して変更を渡せる。

    @mixin box_sizing {
    -moz-box-sizing:border-box;
    -webkit-box-sizing:border-box;
    box-sizing:border-box;
    }
    .
    .
    .
    /* miscellaneous */

    .debug_dump {
      clear: both;
      float: left;
      width: 100%;
      margin-top: 45px;
      @include box_sizing;
    }

この`@include box_sizing`で引っ張ってくる。

## 7.1.2 Usersリソース
ここでRESTを使う。\
データの作成、表示、更新、削除をリソースとして扱うということ。\
HTTP標準にはPOST,GET,PATCH,DELETEが定義されているのでこれらの基本操作を各アクションに割りあてていきましょう。

RESTの原則に従う場合にはリソースへの参照にはユニークIDを使うのが普通。\
ユーザーをリソースとみなす場合id=1のユーザーを参照するということは／User／aというURLに対してGETメソッドをリクエストするということ。\
ここで`show`というアクションは暗黙のリクエストになります。
RailsのREST機能が有効になっていると、GETリクエストは`show`アクションとして扱われる。


現時点では`/users/1`というURLは無効なのでroutesファイルに次の１行を追加する。

    resources :users

この１行を追加すると、RESTfulなUsersリソースで必要となる全てのアクションが利用できるようになるのです。

showページを作成する。

    <%= @user.name %>, <%= @user.email %>


Usersコントローラーには以下を追加する。

    def show
     @user = User.find(params[:id])
    end

GETリクエストから引き渡され、paramsメソッドでidを検索する。

## 7.1.3 debuggerメソッド
Usersコントローラーにdebuggerメソッドを差し込む。

    def show
     @user = User.find(params[:id])
     debugger
    end

この配置ではページに到達した時点でコンソール上でデバッグを行う。

## 7.1.4 Gravator画像とサイドバー
### Gravatar
各ユーザーのプロフィール写真のあたりをもう少し肉付けし、サイドバーも設ける。
この機能は[Gravatar](http://gravatar.com/)
を利用しましょう。\
ユーザーのメールアドレスを組み込んだGravatar専用の画像パスを構成するだけで対応するGravatarの画像が自動的に表示される。

`Gravatar_for`メソッドをヘルパーファイルにおく。
>Rubyでは、Digestライブラリのhexdigestメソッドを使うと、MD5のハッシュ化が実現できます。\
MD5ハッシュでは大文字と小文字が区別されるので、Rubyのdowncaseメソッドを使ってhexdigestの引数を小文字に変換しています。

###サイドバー
show.html.erbに`asideタグ`を使って実装する。このタグはサイドバーなどの補完コンテンツの表示に使われる。\
Bootstrapの一部であるrowクラスとcol-md-4クラスも追加する。


## 7.2 ユーザー登録フォーム
### form_forを使用する
new.html.erbに埋め込みメソッドを利用する。

    <%= form_for(@user) do |f| %>

`do`は`form_for`が一つの変数を持つブロックを取ることを意味している。

このfオブジェクトは@userの属性を設定するために特別に設計されたHTMLを返します

つまり、次のコードを実行すると

    <%= f.label :name %>   
    <%= f.text_field :name %>

Userモデルの`name`属性を設定する、ラベル付きテキストフィールド要素を作成するのに必要なHTMLを作成する。

HTMLでは以下のコードを生成している

    <label for="user_name">Name</label>
    <input id="user_name" name="user[name]" type="text" />

`type="email`ではモバイル端末でアクセスするとメールアドレスに最適化された特別なキーボードが表示されるようになる

## 7.3.1 正しいフォーム
    <form action="/users" class="new_user" id="new_user" method="post">

このHTMLはPOSTリクエストを/usersというURLに送信する。

このフォームを動かすために`users_controller.rb`にコードを追加する。
`render`メソッドを使います。
`render`はコントローラーのアクションの中でも正常に動作します。\
`@user.save`の値がtrueかfalseかで成功失敗の判定を行います。

## 7.3.2 StrongParameter
StrongParameterを使うと必須のパラメーターと許可されたパラメーターを指定することができます。

今回は`params`ハッシュでは`:user`属性を必須とし、名前、メールアドレス、パスワード、パスワードの確認の属性をそれぞれ許可し、それ以外を許可しないようにしないと考えていきます。


以下のような`user_params`というメソッドを作り、対応します。\
`users_controller.rb`に`private`キーワードを使って外部から呼び出せないようにしてしまいましょう。

    params.require(:user).permit(:name, :email, :password, :password_confirmation)

## 7.3.3 エラーメッセージ
今の状態ではユーザー登録で問題が生じてもデバッグ上以外ではエラー内容がわかりません。\
なので画面上に表示されるエラーメッセージを追加しましょう。

`errors.full_messages`オブジェクトは、エラーメッセージの配列を持っています。

保存に失敗すると＠Userオブジェクトに関連づけられたエラーメッセージを追加します。

Rails前派の習慣として、複数の`ビュー`で使われるパーシャルは専用のディレクトリ「`shared`」に保存します。

>パーシャルとは分割したHTMLのようなもの？

## 7.3.4 失敗時のテスト
このテスtとでは、ユーザー登録ボタンを押した時に（ユーザー情報が無効であるために）ユーザーが作成されないことを`count`メソッドを使って確認します。

まず、`get`メソッドを使ってユーザー登録ページにアクセスします。

    get signup_path

フォーム送信をテストするためには、users_pathに対してPOSTリクエストを送信する必要があります。

    assert_no_difference 'User.count' do
        post users_path, params: {user: {
            name:"",
            email:"user@invalid",
            password:"foo",
            password_comfirm:"bar"}}
    end

`create`アクションの`User.new`で期待されているデータを、`params[:user]`というハッシュにまとめています。

## 7.4 ユーザー登録成功
DBへの登録確認とか

## 7.4.1 登録フォームの完成
現状では有効な登録をすると、エラーが発生してしまうのを直しましょう。
ユーザー登録に成功した場合は新しく作成されたユーザーのプロフィールページにリダイレクトするようにしましょう。
以下のメソッドを使用する。

    redirect_to @user

これは以下のコードと同義

    redirect_to user_url(@user)

## 7.4.2 flash
ここで加える変更は、登録完了後に表示されるページにメッセージを表示し、２度目以降にはそのページにメッセージを表示しないようにするというものです。

Railsではこういった情報を変更するためには`flash`という特殊な変数を使います。
この変数はハッシュのように扱います。
Railsの一般的な慣習に倣って`:success`というキーには成功時のメッセージを代入するようにします。

`flash`変数に代入いsたメッセージはリダイレクトした直後のページで表示できるようになります。
今回は`flash`内に存在する事をキーがあるかを調べ、もしあればその値を全て表示するように、レイアウトを修正します。

flash変数の内容をWebサイト全体にわたって表示できるようにすると、次のようなコードになります。

    <% flash.each do |message_type, message| %>
      <div class="alert alert-><%= message_type %>"><%= message %></div>
    <% end %>

適用するCSSクラスをメッセージの種類によって変更するようにしています。\これにより、例えば`:success`キーのメッセージが表示される場合、適用されるCSSクラスは次のようになります。

    alert-success

この時、`:success`キーはシンボルでしたが、テンプレート内に羽根井冴える際に埋め込みRubyが自動的に”`success`”の文字列に置き換わっていることに注意してください。この性質を利用することによってスタイルを動的に変更させることができます。

>  sucessが入るように分岐のパターンを作って表示を切り替えている？？


## 7.4.4 成功時のテスト
ここでいったん、有効な痩身に対するテストを描いてみます。これによって、アプリケーションの振る舞いを検証し、もし今後バグを埋め込まれたらそれを検知できるようになります。

今回は`assert_difference`というめっソドを使ってテストを書きます。

    assert_difference 'User.count',1 do
      post users_path,...
    end

第一引数に文字列を取り、`assert_difference`ブロックないの処理を実行する直前と、実行した直後の`User.count`の値を比較します。
第二引数はオプションですが、ここには比較した結果の差異を渡します。

POSTリクエストをした直後に、`follow_redirect!`というメソッドを使っています。このメソッドはPOSTリクエストを送信した結果を見て、指定されたリダイレクト先に移動するメソッドです。したがって、この行の直後では`users/show`テンプレートが表示されているはずです。


## 7.5 プロのデプロイ
ユーザー登録をセキュアにするために、本番用に青売りkーションに重要な機能を追加していきます。その後、デフォルトのWebサーバを実際の世界で使われているWebサーバに置き換えていきます。

## 7.5.1 本番環境でのSSL
本性で作成したアプリでは送信したデータは流れる途中で捕捉されてしまいます。これを防ぐために[SecureSocketsLayer(SSL)](https://ja.wikipedia.org/wiki/Transport_Layer_Security)を使います。

SSLを有効化するのは簡単です。`production.rb`という本番環境の設定ファイルを１業種性するだけですみます。\
具体的には`config`に「本番環境ではSSLを使うようにする」という設定をするだけ。
config/environments/production.rb

    config.force_ssl = true

次に、サーバーのSSLセットアップを行います。
herokuのサブドメインを使えばSSL証明書の発行を簡素化できる（`herokuの証明書に乗っかる形`）

## 7.5.2 本番環境用のWebサーバー
herokunにお任せ！
DBをマイグレードしよう

## まとめ

- `debugg`メソッドを使うことで、役立つデバッグ情報を表示できる
- saasのmixin機能を使うと、CSSのルールをまとめたり他の場所で変数のように再利用できる
-  Gravatarを使うとユーザーのプロフィール画像を簡単に使用できるようになる。
- `grom_for`ヘルパーはActiveRecordのオブジェクトに対応したフォームを生成する。
- ユーザー登録に失敗した場合にはActiveRecordで検出したエラーを表示するようにした。
- `flash`変数を使うと一時的なメッセージを表示できるよになる。