# Toyアプリケーションの作成
## 2.1 アプリケーションの計画
railsのバージョンとディレクトリ名を指定する。

    $ rails _5.1.6_ new toy_app

### 2.1.1 ユーザーモデル設計
- 重複のない一意の番号を割り振る（id）
- 一般公開されるユーザー名（name）
- メールアドレス（email）

上記3つを持つユーザーを作成する

|user||
|:--:|--|
|id|intger|
|name|string|
|email|string|

## 2.2 Usersリソース
HTTPプロトコル経由で地涌に作成・取得・削除できるオブジェクトと見做すことができるようにする。
scaffoldジェネレーターを使用する。
scaffoldは、`rails generate`スクリプトに`scaffold`コマンドを渡すことで生成される。

### ユーザーはn個のマイクロポストを持っている。
異なるデータモデル同士の関連付けは、Railsの強力な機能。
modelの各要素ファイルにきたすことで実現する。
1つのユーザーに属する

    model/microposts.rb
    $ belong_to :uer

n個のマイクロポストが属する

    model/user.rb
    $ has_many :micropost

### RESTアーキテクチャ
REpresentational State Transferの略。
インターネットやwebアプリケーションのなどの、分散ネットワーク化されたシステムやアプリケーションを構築するためのアーキテクチャスタイルの1つ。
Railsではアプリケーションを構成するコンポーネント（userやmicropostなど）を「リソース」としてモデル化することを指す。

リレーショナルデータベースの
- 作成
- 取得
- 更新
- 削除

HTTP requestメソッド
- post
- get
- patch
- delete

それぞれに対応している。
作成(C)・取得(R)・更新(U)・削除(D)を行うリソースだけでアプリケーション全体を構成することが可能。
