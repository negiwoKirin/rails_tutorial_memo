# 第６章　ユーザーのモデルを作成する
ここから3つの章にわたりユーザー登録ページを作成する。

[Userモデルのモックアップ](https://railstutorial.jp/chapters/images/figures/signup_mockup_bootstrap.png "サンプル")

Railsでは、データモデルのとして扱うデフォルトのデータ構造のことをモデル(Model)と呼びます。
このデータモデルを永続かさせる方法としてデータベースを用いる。
また、Dbとやり取りするRailsライブラリはActiveRecordと呼ばれる。
ActiveRecordは作成/保存/検索のためのメソッドを持っている。
**このメソッドを使うためにSQLを意識する必要はない。**
つまりRailsはデータベースの細部をほぼ完全に隠蔽し、切り離せる。

## 6.1.1 DBの移行
userモデル
|user| | |
|--:|--:|--:|
|id|integer|
|name|string|
|email|string|

コントローラ名には複数形を用い
モデルには単数系を用いる。

**マイグレーションはデータベースに与える変更を定義した`change`メソッドの集まり。**


.find()：
引数と同一のidを検索

.first：
先頭のオブジェクトを検索

.all
全てのオブジェクトを戻り値に

## 6.2.2 存在性の検証
バリデーションで最もポピュラーンなのが存在性（Presence）。
例えばこの節ではemailとnameの検証を行う。

nameの存在性の検証方法

    test "name should be present" do
      @user.name = ""
      assert_not @user.valid?
    end


    validates:name,presense:true


## 6.2.3 長さを検証する
nameの長さ検証

user_test.rb
    test "name should not be too long" do
        @user.name = "a" * 51
        assert_not @user.valid?
    end
user.rb

     validates :name, presence: true, length: { maximum: 50 }

## パスワードのハッシュ化
`railsのhas_secure_password`メソッドを用いる。

